using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombsCollectible : MonoBehaviour
{
     public AudioClip clip;
    public AudioSource source;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();

        controller.gameObject.transform.GetChild(1).gameObject.SetActive(true);
        controller.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Press E to pick up explosives";

        if (controller != null)
        {
            Debug.Log("Pick Up Explosives");
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();

        //Show "Pick up explosives" message

        if (controller != null && controller.isInteract)
        {
            source.GetComponent<AudioSource>().PlayOneShot(clip);
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
            controller.inventory.addItem("Bomb");
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
    }
}
