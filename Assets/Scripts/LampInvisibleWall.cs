using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampInvisibleWall : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        if (controller != null)
        {
            //Show "Lamp required" message
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            controller.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "I can't see anything without a flashlight...";

            Debug.Log("Need Lamp");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
            PlayerController controller = collision.GetComponent<PlayerController>();
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
    }
}