using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FadeAudioSource
{

    public static IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolumeFadeOut, float targetVolumeFadeIn, AudioClip targetAudioClip, PlayerController controller)
    {
        float currentTime = 0;
        float start = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolumeFadeOut, currentTime / duration);
            yield return null;
        }

        controller.audioSource.clip = targetAudioClip;
        controller.audioSource.Play();

        currentTime = 0;
        start = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolumeFadeIn, currentTime / duration);
            yield return null;
        }

        yield break;
    }
}