using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryController 
{

    private Dictionary<string, Item> inventory = new Dictionary<string, Item>();

    public void addItem(string name)
    {
        this.inventory[name].possessed= true;;
       
    }
    
    public bool hasItem(string name)
    {
        return this.inventory[name].possessed;
    }
    public Dictionary<string, Item> getInventory(){
        return inventory;
    }

    public InventoryController(){
        this.inventory.Add("Note1",new Item("Note1", "Boulder", "I'm scared. Don't wanna see it again. But as long as I remember them I'm fine, right ?", "Note",false));
        this.inventory.Add("Note2",new Item("Note2","Picture", "The therapist told me. They're with me. I can see them whenever I want. They're in nearby rooms.","Note",false));
        this.inventory.Add("Note3",new Item("Note3", "Cars", "Hate them. Too fast. Pay no attention. Red stain on the road. No punishment whatsoever.","Note",false));
        this.inventory.Add("Note4",new Item("Note4", "Axe", "Foolish child. Didn't see where he ran. Dumb accident. Scary. Lucky that it wasn't lethal...","Note",false));
        this.inventory.Add("Note5",new Item("Note5", "Fire", "Hurts. So much. So long ago but still afraid. Cannot come closer...","Note",false));
        this.inventory.Add("Note6",new Item("Note6", "Gun", "He was a hunter and a lumberjack. The latter almost accidentaly cost him someone dear. Former almost did, on purpose though. Why did he not talk to me ? Now he's scarred for life.","Note",false));
        this.inventory.Add("Note7",new Item("Note7", "Lock", "This part of my mind is private, got that ? Yougsters these days I swear, when I was younger we had some respect for our elders.","Note",false));
        this.inventory.Add("Lamp",new Item("Lamp","Flashlight","A very bright flashlight.","Lampe",false));
        this.inventory.Add("Bomb",new Item("Bomb","Explosive","Some explosives for underwater exploration.","Tnt",false));

    }

}
