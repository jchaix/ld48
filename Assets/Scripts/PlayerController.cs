using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private float movX;
    private float movY;
    public float speed = 1;
    public bool isInteract;
    private Rigidbody2D rb;
    public InventoryController inventory = new InventoryController();
    Animator animator;
    float lookDirection;
    public bool hasLamp = false;

    public GameObject inventoryPanel;
    public GameObject mainmenuPanel;
    public GameObject descriptionPanel;
    public GameObject finalNotePanel;
    bool InventoryOpen;
    bool PauseMenuOpen;

    public float spawn_depth_offset = 5;
    public int initial_temp = 20;

    public AudioSource audioSource;
    public AudioClip zone1;
    public AudioClip zone2;
    public AudioClip zone3;

    //public Dictionary<string, bool> items_collection = new Dictionary<string, bool>();
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        //inventoryPanel = GameObject.Find("Inventory");
        InventoryOpen = false;
        PauseMenuOpen = false;
        audioSource.clip = zone1;
        audioSource.Play();
    }


    // Update is called once per frame
    void Update()
    {
        //Movement
        movX = Input.GetAxis("Horizontal");
        movY = Input.GetAxis("Vertical");
        isInteract = Input.GetButton("Interact");

        Vector2 move = new Vector2(movX, movY);
        if (!Mathf.Approximately(move.x, 0.0f))
        {
            lookDirection = move.x;
        }
        //Debug.Log(lookDirection);
        animator.SetFloat("Mov X", lookDirection);
        animator.SetBool("Has Lamp", hasLamp);
        if (hasLamp)
        {
            gameObject.transform.GetChild(2).gameObject.SetActive(true);
        }

        if (Input.GetButtonDown("Inventory")) {
            Debug.Log("il a cliqué");
            InventoryOpen = !InventoryOpen;
            toggleInventory();
        }

        if (Input.GetButtonDown("Menu")){
            Debug.Log("Open Pause Menu");
            PauseMenuOpen = !PauseMenuOpen;
            toggleMainMenu();
        }

        if(inventory.hasItem("Note1") && inventory.hasItem("Note2") && inventory.hasItem("Note3") && inventory.hasItem("Note4") && inventory.hasItem("Note5") && inventory.hasItem("Note6") && inventory.hasItem("Note7"))
        {
            Debug.Log("Got all notes");
            GameObject[] gameObjects = gameObject.scene.GetRootGameObjects();
            foreach (GameObject gmo in gameObjects)
                if (gmo.name == "NoteFinal")
                    gmo.SetActiveRecursively(true);
        }
    }

    private void FixedUpdate()
    {
        Vector2 pos = rb.position;
        pos.x = pos.x + movX * speed * Time.deltaTime;
        pos.y = pos.y + movY * speed * Time.deltaTime;
        //Debug.Log(pos);
        //Debug.Log(inventory.hasItem("Bomb"));
        //Debug.Log("Interact: "+isInteract);
        rb.MovePosition(pos);

        mainmenuPanel.transform.Find("Time_minutes").GetComponent<TMPro.TextMeshProUGUI>().text = Mathf.FloorToInt(Time.time / 60).ToString("00");
        mainmenuPanel.transform.Find("Time_seconds").GetComponent<TMPro.TextMeshProUGUI>().text = Mathf.FloorToInt(Time.time % 60).ToString("00");
        mainmenuPanel.transform.Find("Temperature").GetComponent<TMPro.TextMeshProUGUI>().text = (initial_temp - Mathf.FloorToInt(-(gameObject.transform.position.y) / 15)).ToString("00");
        mainmenuPanel.transform.Find("Depth").GetComponent<TMPro.TextMeshProUGUI>().text = Mathf.FloorToInt(-(gameObject.transform.position.y) + spawn_depth_offset).ToString("000");
    }

    public void displayInventory(){

        foreach (var item in this.inventory.getInventory())
        {
           // Debug.Log(item.Value.name +" : " + item.Value.asset);
            if(!item.Value.possessed){
                if(!item.Value.asset.Contains("lock")){
                    item.Value.asset = item.Value.asset+"lock";
                    inventoryPanel.transform.Find(item.Key).GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "???";
                }
            }else{
                if(item.Value.asset.Contains("lock")){
                    string asset = item.Value.asset;
                    asset = asset.Replace("lock","");
                    item.Value.asset = asset;
                }
                inventoryPanel.transform.Find(item.Key).GetComponentInChildren<TMPro.TextMeshProUGUI>().text = item.Value.title;
            }
            updateInventoryImage(item.Key,item.Value.asset);
        }
    }

    public void updateInventoryImage( string name, string asset){
        Debug.Log(name +" : " + asset);
        Debug.Log(inventoryPanel.ToString());
        var img = inventoryPanel.transform.Find(name).GetComponent<Image>();
        Debug.Log(img.ToString());
        img.sprite = Resources.Load<Sprite>(asset);
   }

    public void showItemDescription(string name)
    {
        Dictionary<string, Item> getinventory = inventory.getInventory();

        if (getinventory[name].possessed)
        {
            descriptionPanel.transform.Find("DescriptionText").GetComponent<TMPro.TextMeshProUGUI>().text = getinventory[name].description;
            inventoryPanel.SetActive(false);
            descriptionPanel.SetActive(true);
        }
    }

    public void returnToInventory()
    {
        descriptionPanel.SetActive(false);
        toggleInventory();
    }

    public void toggleInventory(){
      Debug.Log("il a toggle");
      displayInventory();
      inventoryPanel.SetActive(InventoryOpen);
   }

    public void toggleMainMenu()
    {
        Debug.Log("Toggle Main Menu");
        mainmenuPanel.SetActive(PauseMenuOpen);
    }
}
