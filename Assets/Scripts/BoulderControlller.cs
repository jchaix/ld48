using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderControlller : MonoBehaviour
{

    public AudioClip clip;
    public AudioSource source;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();


        if (controller != null && !controller.inventory.hasItem("Bomb"))
        {
            //Show "Need Bombs" message
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            controller.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "I need some explosives to clear these boulders...";
            Debug.Log("Need Explosives");
        }        

        if (controller != null && controller.inventory.hasItem("Bomb"))
        {
            //Show "Destroy Boulder" message
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            controller.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Press E to place the explosives";
            Debug.Log("Destroy boulders");
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();

        if (controller != null && controller.inventory.hasItem("Bomb"))
        {
            if (controller.isInteract)
            {
                source.GetComponent<AudioSource>().PlayOneShot(clip);

                controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
                GameObject[] gameObjects = controller.gameObject.scene.GetRootGameObjects();
                foreach (GameObject gmo in gameObjects)
                    if (gmo.name == "Explosions")
                        gmo.SetActive(true);      
                
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
    }

}
