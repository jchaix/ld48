using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteCollectable : MonoBehaviour
{
    public string noteId;

     public AudioClip clip;
    public AudioSource source;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        if (controller != null)
        {
            //Show "Pick up note X" message
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            controller.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Press E to pick up the note";
            Debug.Log("Pick Up "+noteId);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        if (controller != null && controller.isInteract)
        {
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
            controller.inventory.addItem(noteId);
            source.GetComponent<AudioSource>().PlayOneShot(clip);
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
    }

}
