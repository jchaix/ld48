using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item
{
    public string name;

    public string title;
    public string description;
    public string asset;

    public bool possessed;
   
    public Item(string name,string title, string description, string asset, bool possessed){
        this.name = name;
        this.title = title;
        this.description = description;
        this.asset = asset;
        this.possessed = possessed;
    }
    
}
