using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBackgroundTrack : MonoBehaviour
{
    public string collisionZone;
    public static float fadeDuration = 0.5f;

    private IEnumerator fadeOutIn;
    private AudioClip currentAudioClip;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();

        currentAudioClip = controller.audioSource.clip;
        if(collisionZone == "zone1" && currentAudioClip == controller.zone1)
        {
            fadeOutIn = FadeAudioSource.StartFade(controller.audioSource, fadeDuration, 0, 0.05f, controller.zone2, controller);
            StartCoroutine(fadeOutIn);
        }
        else if(collisionZone == "zone1" && currentAudioClip == controller.zone2)
        {
            fadeOutIn = FadeAudioSource.StartFade(controller.audioSource, fadeDuration, 0, 0.05f, controller.zone1, controller);
            StartCoroutine(fadeOutIn);
        }
        else if(collisionZone == "zone3" && currentAudioClip == controller.zone2)
        {
            fadeOutIn = FadeAudioSource.StartFade(controller.audioSource, fadeDuration, 0, 0.05f, controller.zone3, controller);
            StartCoroutine(fadeOutIn);
        }
        else if(collisionZone == "zone3" && currentAudioClip == controller.zone3)
        {
            fadeOutIn = FadeAudioSource.StartFade(controller.audioSource, fadeDuration, 0, 0.05f, controller.zone2, controller);
            StartCoroutine(fadeOutIn);
        }
    }
}