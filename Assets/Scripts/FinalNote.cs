using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalNote : MonoBehaviour
{
    public AudioClip clip;
    public AudioSource source;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();

        if (controller != null && controller.inventory.hasItem("Note1") && controller.inventory.hasItem("Note2") && controller.inventory.hasItem("Note3") && controller.inventory.hasItem("Note4") && controller.inventory.hasItem("Note5") && controller.inventory.hasItem("Note6") && controller.inventory.hasItem("Note7"))
        {
            //Show "Pick up note X" message
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            controller.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Press E to pick up the final note";
            Debug.Log("Pick Up Last Note");
        }
        else
        {
            //Show "Nedd all notes" message
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            controller.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "I should find all the notes...";
            Debug.Log("Pick Up All Notes");
        }
    }

        private void OnTriggerStay2D(Collider2D collision)
        {
            PlayerController controller = collision.GetComponent<PlayerController>();
            if (controller != null && controller.isInteract && controller.inventory.hasItem("Note1") && controller.inventory.hasItem("Note2") && controller.inventory.hasItem("Note3") && controller.inventory.hasItem("Note4") && controller.inventory.hasItem("Note5") && controller.inventory.hasItem("Note6") && controller.inventory.hasItem("Note7"))
            {
                controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
                source.GetComponent<AudioSource>().PlayOneShot(clip);
                Destroy(gameObject);
                //Show last message and ending screen
                controller.finalNotePanel.transform.Find("DescriptionText").GetComponent<TMPro.TextMeshProUGUI>().text = "In a matter of minutes I'll wake up a new woman. Lighter. Free. I may not have decades to live, but I'll be fine now. Thank you.";
                controller.finalNotePanel.SetActive(true);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            PlayerController controller = collision.GetComponent<PlayerController>();
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        }
}
