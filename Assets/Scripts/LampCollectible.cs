using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampCollectible : MonoBehaviour
{
    public AudioClip clip;
    public AudioSource source;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        if (controller != null)
        {
            //Show "Pick up lamp" message
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            controller.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Press E to pick up the flashlight";
            Debug.Log("Pick Up Lamp");
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        if (controller != null && controller.isInteract)
        {
            source.GetComponent<AudioSource>().PlayOneShot(clip);
            controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
            controller.inventory.addItem("Lamp");
            Destroy(gameObject);
            controller.hasLamp = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController controller = collision.GetComponent<PlayerController>();
        controller.gameObject.transform.GetChild(1).gameObject.SetActive(false);
    }
}
