using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManageScenes : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MainMenu()
    {
        Debug.Log("Change Scene");
        SceneManager.LoadScene("MainMenu");
    }

    public void PlayScene()
    {
        SceneManager.LoadScene("PlayScene");
    }
}
